<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
    <style>
    body {
        width           : 100%;
        background-color: purple;
        text-align      : center;
        font-size       : 25px;}

    .container {
        width          : 90%;
        display        : flex;
        justify-content: space-around;
        margin         : 0 auto;
        flex-direction: row; }

    .container .bloc {
        padding: 0 20px;}

    .bloc {
        background-color: white;
        box-shadow      : 0 0 7px grey;
        text-align      : center;
        justify-items   : center;
        padding         : 10px;
        border-radius   : 10px; }

    .taille {
        width    : 75%;
        margin   : 0 auto;
        font-size: 30px; }

    p {
        font-family: Arial, Helvetica, sans-serif;}

    @media (max-width:700px){

        .container{
            flex-direction: column;
        }

        .container .bloc {
            padding: 10px;
            margin: 10px 0;
        }

        .br {
            display:none;
        }

        .taille {
            width    : 85%;
            font-size: 25px;
        }
    }
    </style>
</head>
    <body>
            
        <?php
    
             // Créer un script afin de récupérer toutes les infos de l'Ordonnance.txt qu'a rempli le Dr Strauss
             // Récupérer les coordonnées du patient, celle du docteur, son hopital, la date, les médicaments et la personne qui à fait l'ordonnance
             // Affichez comme y faut les informations dans une jolie page html bien stylisée
             // Lorsque que vous pensez avoir fini, venez me voir avec votre pc que je test votre code
            
        ?>
        
        <!-- écrire le code après ce commentaire -->
            
            	<?php
          //ouvrir le dossier//           
        $source = fopen('Ordonnance.txt', 'rb');  
       
        // recuperer les infos //

            for ($i=0; $i < 3; $i++) { 
        
         // on laisse un vide//
        $vide = fgets($source);
                }
                
            $nomPrenom = fgets($source);
            $rue = fgets($source);
            $CPVille = fgets($source);

        $vide = fgets($source);
              
            $medecin = fgets($source);
            $hopital = fgets($source);


            $datebrute=fgets($source);
            $date = str_replace(',','',$datebrute);
            $datebrute=str_replace('Le','',$date);

        $vide = fgets($source);
                    
            $nombreMedicamenttotal = fgets($source);
            //on ecrit et remplace le nom du medicament//

            $nombreMedicament = str_replace('Nombres de médicaments : ', '', $nombreMedicamenttotal);
                
            for ($i=0; $i < 3; $i++) { 

        $vide = fgets($source);
            }
        
            //la variable $i est compare aux medicaments et on recupere la liste des medicaments//

                $listeMedocBrute='';

                for ($i=0; $i < $nombreMedicament; $i++) { 

                 $listeMedocBrute .= fgets($source).'<br>';
                        }

                $listeMedoc = str_replace('- ', '', $listeMedocBrute);

        $vide = fgets($source);

                $redacOrdo = fgets($source);
                
    
      ?>
        
                <h1>Ordonnance</h1>
                
                <div class="container">
                    <div class="bloc">
                        <p>Information patient :</p>
                        <p><?php echo $nomPrenom; ?></p>
                        <p><?php echo $rue; ?></p>
                        <p><?php echo $CPVille; ?></p>
                    </div>

                    <div class="bloc">
                        <p>Informations médecin</p>
                        <span class="br"><br></span>
                        <p><?php echo $medecin; ?></p>
                        <p><?php echo $hopital; ?></p>
                    </div>
                </div>

                <div class="container"><p>Date : <?php echo $date; ?></p></div>

                <div class="bloc taille"><p><?php echo $listeMedoc; ?></p></div>

                <div class="container"><p><?php echo $redacOrdo; ?></p></div>

        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>